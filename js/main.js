let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

function getEle(tag) {
  return document.querySelector(tag);
}

let renderListGlasses = () => {
  let htmlGlasses = ``;
  dataGlasses.forEach((item) => {
    let glass = `<img class="col-4 style="width: 100%;" src='${item.src}' alt="" />`;
    htmlGlasses += glass;
  });
  getEle("#vglassesList").innerHTML = htmlGlasses;
};

renderListGlasses();

let infoGlass = (item) => {
  return `
  <div>
  <p style="font-weight: bold"> ${item.name} - ${item.brand} (${item.color})</p> 
  <p><span style="padding: 5px 5px; background:red; color:white ">$ ${item.price} </span>
  <span style="margin-left:5px; color:green"> Stocking</span></p>
  <p>${item.description}</p>
  </div>
`;
};

let imgGlass = (item) => {
  return `<img src='${item.virtualImg}' alt=""/>`;
};

let imgGlassList = getEle("#vglassesList").querySelectorAll("img");
imgGlassList.forEach((item, i) => {
  item.onclick = () => {
    getEle(".vglasses__model img").style.display = "block";
    getEle(".vglasses__info").style.display = "block";
    let indexGlass = dataGlasses[i];
    let changeGlass = imgGlass(indexGlass);
    let infoGlassChange = infoGlass(indexGlass);
    getEle("#avatar").innerHTML = changeGlass;
    getEle("#glassesInfo").innerHTML = infoGlassChange;
  };
});

function removeGlasses(flag) {
  if (flag === true) {
    getEle(".vglasses__model img").style.display = "none";
  } else {
    getEle(".vglasses__model img").style.display = "block";
  }
}

window.removeGlasses = removeGlasses;
